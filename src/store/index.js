import Vue from 'vue'
import Vuex from 'vuex'

import todo from './modules/todo.js'

Vue.use(Vuex)

const store = new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    todo,
  },
})

export default store
