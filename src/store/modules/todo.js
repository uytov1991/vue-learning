export default {
  state: () => ({
    todos: [
      { id: 1, title: 'buy something', done: true },
      { id: 2, title: 'buy all', done: false },
      { id: 3, title: 'buy apple', done: true },
      { id: 4, title: 'buy pine', done: true },
    ],
    id: 5,
  }),
  mutations: {
    deleteTask(state, { id }) {
      state.todos = state.todos.filter((el) => el.id !== id)
    },
    addTask(state, { title }) {
      state.todos.unshift({ id: state.id++, title, done: false })
    },
    checkTask(state, { id }) {
      let checkedTask = state.todos.find((el) => el.id === id)
      checkedTask.done = !checkedTask.done
    },
    deleteCompleteTasks(state) {
      state.todos = state.todos.filter((elem) => !elem.done)
    },
    checkAllTasks(state) {
      state.todos = state.todos.map((el) => {
        el.done = true
        return el
      })
    },
  },
  actions: {
    deleteTask({ commit }, payload) {
      commit('deleteTask', payload)
    },
    checkTask({ commit }, payload) {
      commit('checkTask', payload)
    },
    addTask({ commit }, payload) {
      commit('addTask', payload)
    },
  },
  getters: {
    allTasks(state) {
      return state.todos.length
    },
    doneTasks(state) {
      return state.todos.filter((el) => el.done).length
    },
  },
}
